#!/bin/bash
swaylock --hide-keyboard-layout \
    --indicator-caps-lock \
    --clock \
    --text-color D8DEE9 \
    --ring-color 2E3440 \
    --ring-clear-color EBCB8B \
    --ring-wrong-color BF616A \
    --text-ver-color 5E81AC \
    --indicator-radius 110 \
    --indicator-thickness 8 \
    --inside-color 4C566A \
    --inside-clear-color 4C566A \
    --key-hl-color ECEFF4 \
    --grace 3 \
    -f \
    --fade-in 1
